//
//  TestViewController.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

protocol TestSymbolSelectDelegate {
    func selectSymbol(_ button: AnswerButton?)
}

class TestViewController: UIViewController, Storyboarded {

    @IBOutlet weak var testSymbolView: TestView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var testProgressView: UIProgressView!
    @IBOutlet weak var testCountLabel: UILabel!
    @IBOutlet weak var answerIndicatorView: UIView!
    @IBOutlet weak var answerIndicatorImageView: UIImageView!
    @IBOutlet weak var answerStateLabel: UILabel!
    @IBOutlet weak var titleQuestionLabel: UILabel!
    
    public var viewModel: TestViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.barStyle = .default
        
        roundProgressView(testProgressView)
        
        testSymbolView.selectDelegate = self
        testSymbolView.flagImageView.image = viewModel.languageFlagImage
        
        
        updateSymbolCard()
        
        testCountLabel.text = "\(viewModel.answeredCount)/\(viewModel.testCount)"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.selectedAnswer = { answerViewState in
            self.updateAnswerStateView(with: answerViewState)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func dismissButtonTouched(_ sender: UIButton) {
        viewModel.dismiss()
    }
    
    private func updateSymbolCard() {
        guard let symbolOnTest = viewModel.symbolOnTest else {
            viewModel.complete()
            return
        }
        
        testSymbolView.symbolLabel?.text = "\(viewModel.symbolCapital)  \(viewModel.symbolNormal)"
        
        for (index, button) in testSymbolView.answerButtonCollection.enumerated() {
            let variant = viewModel.getSymbolVariant(for: symbolOnTest, atVariant: index)
            button.setupSymbolLabel(with: variant)
            button.isAnswered = false
            button.isSelected = false
        }
    }
    
    private func roundProgressView(_ progressView: UIProgressView) {
        let neededHeight: CGFloat = 6
        let yScale  = neededHeight / progressView.frame.height
        progressView.transform = testProgressView.transform.scaledBy(x: 1, y: yScale)
        progressView.rounded()
        progressView.layer.sublayers![1].cornerRadius = neededHeight / 2
        progressView.subviews[1].clipsToBounds = true
    }
    
    public func updateAnswerStateView(with answerState: AnswerStateViewType) {
        UIView.animate(withDuration: 0.4) {
            self.answerIndicatorView.alpha = answerState == .pending ? 0 : 1.0
        }
        
        if answerState != .pending {
            answerIndicatorImageView.image = answerState.image
            answerStateLabel.text = answerState.title
            answerStateLabel.textColor = answerState.titleTextColor
            answerIndicatorView.backgroundColor = answerState.backgroundColor
            answerIndicatorView.layer.borderWidth = 12
            answerIndicatorView.layer.borderColor = answerState.borderColor
            answerIndicatorView.rounded()
        }
    }

}

extension TestViewController: TestSymbolSelectDelegate {

    func selectSymbol(_ button: AnswerButton?) {
        testProgressView.progress += 1 / Float(viewModel.testCount)
        viewModel.answeredCount += 1
        testCountLabel.text = "\(viewModel.answeredCount)/\(viewModel.testCount)"
        viewModel.selectSymbol(button)
        testSymbolView.answerButtonCollection.forEach {
            $0.isAnswered = viewModel.isAnswered
            $0.isHidden = !$0.isVariantCorrect()
            $0.isEnabled = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self = self else { return }
            
            self.testSymbolView.answerButtonCollection.forEach {
                $0.isHidden = false
                $0.isEnabled = true
            }
            button?.isSelected = false
            self.viewModel.isAnswered = false
            self.updateAnswerStateView(with: .pending)
            self.updateSymbolCard()
        }
    }
}
