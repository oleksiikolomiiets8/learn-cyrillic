//
//  TestViewModel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit
import Foundation

protocol TestViewModelProtocol: ViewModel  {
    var languageFlagImage: UIImage? { get }
    var dismiss: () -> Void { get set }
    var testComplete: (_: [Symbol: [VariantProtocol]]) -> Void { get set }
    var symbolCapital: String { get }
    var symbolNormal: String { get }
    var learnedSymbolsCount: Int { get }
    var testCount: Int { get }
    
    func selectSymbol(_ symbolButton: AnswerButton?)
}

final class TestViewModel {
    
    private var updateCallback: ViewModelCallback = nil
    private let symbolsHelper: SymbolsHelper
    private let testingLanguage: LanguageType
    
    public var answerType: AnswerStateViewType = .negative
    public var dismiss: () -> Void = { print("dismiss not overriden") }
    public var selectedAnswer: (_: AnswerStateViewType) -> Void = { _ in print("selectedAnswer not overriden") }
    public var testComplete: (_: [Symbol: [VariantProtocol]]) -> Void = { _ in print("testComplete not overriden") }
    public var symbolOnTest: Symbol!
    private var testSymbols: [Symbol: [VariantProtocol]]!
    private var symbolsAfterTest = [Symbol: [VariantProtocol]]()
    public var isAnswered: Bool = false
    public var answeredCount = 0
   
    init(with helper: SymbolsHelper, _ language: LanguageType) {
        symbolsHelper = helper
        testingLanguage = language
        testSymbols = symbolsHelper.getRandomSymbols(atCode: language)
        symbolOnTest = Array(testSymbols.keys).first
    }
    
    func nextSymbol(previous: Symbol) -> Symbol? {
        let symbols = Array(testSymbols.keys)
        let nextIndex = symbols.index(after: symbols.firstIndex(of: previous)!)
        if nextIndex > symbols.count - 1 {
            return nil
        }
        return symbols[nextIndex]
    }
    
    func getSymbolVariant(for symbol: Symbol, atVariant index: Int) -> VariantProtocol {
        return testSymbols[symbol]![index]
    }
    
    func complete() {
        testComplete(symbolsAfterTest)
    }
    
}

extension TestViewModel: TestViewModelProtocol {
    
    var learnedSymbolsCount: Int {
        return symbolsHelper.getLearnedCount(for: testingLanguage)
    }
    
    var symbolCapital: String {
        return symbolOnTest.cyrillicCaps
    }
    
    var languageFlagImage: UIImage? {
        return UIImage(named: testingLanguage.rawValue)
    }
    
    var symbolNormal: String {
        return symbolOnTest.cyrillicNormal
    }
    
    var testCount: Int {
        return testSymbols.count
    }

    func subscribe(with updateCallback: ViewModelCallback) {
        self.updateCallback = updateCallback
    }
    
    func selectSymbol(_ symbolButton: AnswerButton?) {
        guard let isCorrect = symbolButton?.isVariantCorrect(),
            let answer = AnswerStateViewType(isCorrect) else { return }
        
        let state = LearningState(isCorrect)
        symbolsHelper.setLearningState(state, for: testingLanguage, symbolOnTest)
        
        let variants = testSymbols[symbolOnTest]
        symbolOnTest.state = state
        
        symbolsAfterTest[symbolOnTest] = variants
        
        var element = testSymbols.keys.first(where: { $0 == symbolOnTest })!
        element.state = state
        
        isAnswered = true
        selectedAnswer(answer)
        symbolOnTest = nextSymbol(previous: symbolOnTest)
    }
}
