//
//  HomeViewModel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/1/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit
import Foundation

protocol HomeViewModelProtocol: ViewModel {
    var navBarButtonRatioSize: CGSize { get }
    var symbolCount: Int { get }
    var currentSymbolIndex: Int { get }
    var normalSymbols: [String] { get }
    var languageImage: UIImage? { get }
    var difficaltyImage: UIImage? { get }
    var infoImage: UIImage? { get }
    var nextSymbolIndex: Int { get }
    
    var selectLanguage: (() -> Void) { get set }
    var lastCompletedTest: (() -> Void) { get set }
    var showInfo: (() -> Void) { get set }
    var testYourKnowledge: ((_ :LanguageType) -> Void) { get set }
    
    func loadSymbols()
    func toggleSymbolsOrder()
    func test()
    func symbolCapital(at index: Int) -> String
    func symbolNormal(at index: Int) -> String
    func symbolTransliteration(at index: Int) -> String
    func symbolEnglishPronunciation(at index: Int) -> String
    func originalExample(at index: Int) -> String
    func translatedExample(at index: Int) -> String
    func learningState(at index: Int) -> LearningState
    func updateSelectedIndex(_ index: Int)
    func changeLanguage(to language: LanguageType)
    func updateSymbolState(_ state: LearningState)
}

fileprivate class Config {
    static let kButtonSize: CGSize = CGSize(width: 32, height: 24)
    static let kSelectedSymbolIndex: Int = 0
}

final class HomeViewModel {
    private var symbols = [Symbol]()
    private var symbolsOrder: SymbolOrder = .ascending
    private var languageType: LanguageType
    private var updateCallback: ViewModelCallback = nil
    private let symbolsHelper: SymbolsHelper
    private var selectedIndex = Config.kSelectedSymbolIndex
    
    public var selectLanguage: (() -> Void) = { print("selectLanguage not overriden") }
    public var testYourKnowledge: ((_ :LanguageType) -> Void) = { _ in print("testYourKnowledge not overriden") }
    public var showInfo: (() -> Void) = { print("showInfo not overriden") }
    public var lastCompletedTest: (() -> Void) = { print("lastCompletedTest not overriden") }
    
    init(with helper: SymbolsHelper, _ language: LanguageType) {
        symbolsHelper = helper
        languageType = language
    }
}

extension HomeViewModel: HomeViewModelProtocol {
    
    public var navBarButtonRatioSize: CGSize {
        return Config.kButtonSize
    }
    
    public var symbolCount: Int {
        return symbols.count
    }
    
    public func test() {
        testYourKnowledge(languageType)
    }
    
    public var currentSymbolIndex: Int {
        return selectedIndex > symbolCount - 1 ? symbolCount - 1 : selectedIndex
    }
    
    public var normalSymbols: [String] {
        return symbols.map { $0.cyrillicNormal }
    }
    
    public var languageImage: UIImage? {
        return UIImage(named: languageType.rawValue)
    }
    
    public var difficaltyImage: UIImage? {
        return UIImage(named: symbolsOrder.buttonTitle)
    }
    
    public var infoImage: UIImage? {
        return UIImage(named: "info")
    }
    
    public var nextSymbolIndex: Int {
        return currentSymbolIndex + 1 > symbolCount - 1 ? 0 : currentSymbolIndex + 1
    }
    
    func subscribe(with updateCallback: ViewModelCallback) {
        self.updateCallback = updateCallback
        loadSymbols()
    }
    
    func loadSymbols() {
        symbols = symbolsHelper.getSymcolsBylanguageCode(atCode: languageType, orderBy: symbolsOrder) ?? []
        updateCallback!(nil)
    }
    
    func toggleSymbolsOrder() {
        switch symbolsOrder {
        case .ascending:
            symbolsOrder = .difficulty
        case .difficulty:
            symbolsOrder = .ascending
        }
        loadSymbols()
    }
    
    func symbolCapital(at index: Int) -> String {
        return symbols[index].cyrillicCaps
    }
    
    func symbolNormal(at index: Int) -> String {
        return symbols[index].cyrillicNormal
    }
    
    func symbolTransliteration(at index: Int) -> String {
        return symbols[index].transliteration
    }
    
    func symbolEnglishPronunciation(at index: Int) -> String {
        return "\(symbols[index].englishPronunciation) \(symbols[index].pronunciation)"
    }
    
    func originalExample(at index: Int) -> String {
        return symbols[index].examples.first?.cyrillic ?? "no_data".localized
    }
    
    func translatedExample(at index: Int) -> String {
        return symbols[index].examples.first?.englishTranslation ?? "no_data".localized
    }
    
    func learningState(at index: Int) -> LearningState {
        return symbols[index].state
    }
    
    func updateSelectedIndex(_ index: Int) {
        selectedIndex = index
    }
    
    func changeLanguage(to language: LanguageType) {
        languageType = language
        loadSymbols()
    }
    
    func updateSymbolState(_ state: LearningState) {
        symbolsHelper.setLearningState(state, for: languageType, symbols[currentSymbolIndex])
    }
}
