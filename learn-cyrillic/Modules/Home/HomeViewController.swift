//
//  ViewController.swift
//  learn-cyrillic
//
//  Created by Olexander Markov on 1/9/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

fileprivate class HomeSettings {
    static let kTopBackFrontSymbolsConstraint: CGFloat = 16
    static let kMaxMoveCardAngle = CGFloat(Double.pi / 6)
    static let kBackCardMinScale: CGFloat = 0.85
}

class HomeViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var frontSymbolCardView: SymbolCardView!
    @IBOutlet weak var backSymbolCardView: SymbolCardView!
    @IBOutlet weak var symbolsStackView: UIStackView!
    @IBOutlet weak var testButton: UIButton!
    @IBOutlet weak var previewLabel: UILabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var yesLabel: UILabel!
    @IBOutlet weak var swipeGestureView: UIView!
    
    private var languageButton = UIButton()
    private var difficaltyButton = UIButton()
    private var infoButton = UIButton()
    private var yesAnimation: UIViewPropertyAnimator!
    private var noAnimation: UIViewPropertyAnimator!
    
    private var leftRotation: UIViewPropertyAnimator!
    private var rightRotation: UIViewPropertyAnimator!
    
    private var moveRight: UIViewPropertyAnimator!
    private var moveLeft: UIViewPropertyAnimator!
    
    private var scaleDownAndMove: UIViewPropertyAnimator!
    private var start: CGPoint!
    
    private var pannedSymbol: Int!
    public var viewModel: HomeViewModelProtocol!
    public var chosenLangugage: LanguageType!
    private var panGestureLength: CGFloat!
    
    private var isBackCardAnimated = true
    private var translationValue: CGFloat = 0.0
    
    private var foregroundObserver: NSObjectProtocol?
    private var backgroundObserver: NSObjectProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleView = UILabel.navBarTitleView(title: "Learn Cyrillic")
        
        setupNavBarButtons()
        testButton.rounded()
        testButton.shadowed()
        testButton.imageEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        noLabel.rounded()
        yesLabel.rounded()
        zoomOutHelpLabels()
        
        viewModel.subscribe { [weak self] error in
            guard let self = self else { return }
            DispatchQueue.main.async {
                guard error == nil else {
                    self.displayAlert(title: "error".localized, msg: error!.localizedDescription)
                    return
                }
                
                self.setupSymbols()
                
                self.setupSymbolCards()
                
                self.languageButton.setImage(self.viewModel.languageImage, for: .normal)
            }
        }
        
        setupSymbolCards()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(moveCard(_ :)))
        view.addGestureRecognizer(panGesture)
        
        let symbolsPanGesture = UIPanGestureRecognizer(target: self, action: #selector(panSymbolsView(_ :)))
        symbolsPanGesture.minimumNumberOfTouches = 1
        symbolsStackView.addGestureRecognizer(symbolsPanGesture)
        
        foregroundObserver = NotificationCenter.default
            .addObserver(forName: UIApplication.willEnterForegroundNotification,
                         object: nil,
                         queue: .main) { [unowned self] _ in
                            self.setupYesAndNoAnimation()
                            self.setupRotationAnimation()
        }
        
        backgroundObserver = NotificationCenter.default
            .addObserver(forName: UIApplication.didEnterBackgroundNotification,
                         object: nil,
                         queue: .main) { [unowned self] _ in
                            self.stopYesAndNoAnimation()
                            self.stopRotationAnimation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.loadSymbols()
        setupSymbols()
        setupYesAndNoAnimation()
        setupRotationAnimation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if isBackCardAnimated {
            isBackCardAnimated = false
            
            start = frontSymbolCardView.center
            
            AnimatorFactory.scaleAndMove(view: backSymbolCardView, to: HomeSettings.kBackCardMinScale).startAnimation()
        }
        
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopYesAndNoAnimation()
        stopRotationAnimation()
    }
    
    @IBAction func testButtonTouched(_ sender: UIButton) {
        viewModel.test()
    }
    
    @objc func changeLanguage(_ sender: UIButton) {
        viewModel.selectLanguage()
    }
    
    @objc func changeOrder(_ sender: UIButton) {
        viewModel.toggleSymbolsOrder()
        sender.setImage(viewModel.difficaltyImage, for: .normal)
        setupSymbolCards()
        setupSymbols()
    }
    
    @objc func infoButtonTouched(_ sender: UIButton) {
        let infoActions = [UIAlertAction(title: "сancel".localized, style: .cancel, handler: nil),
                           .defaultAction("last".localized) { _ in self.viewModel.lastCompletedTest() },
                           .defaultAction("select_language".localized) { _ in self.viewModel.selectLanguage() },
                           .defaultAction("rate".localized) { _ in self.displayAlert(title: "Rate", msg: "Rate") },
                           .defaultAction("about".localized) { _ in self.viewModel.showInfo() } ]
        displayActions(infoActions)
    }
    
    @objc private func moveCard(_ gesture: UIPanGestureRecognizer) {
        let piece = gesture.view!
        let gestureTranslation = gesture.translation(in: piece.superview).x
        let signCoeficient: CGFloat = gestureTranslation.sign == .minus ? -1 : 1
        
        translationValue = gestureTranslation / piece.superview!.frame.width
        
        if abs(translationValue) <= 0.4 {
            frontSymbolCardView.center.x = start.x + gestureTranslation
            frontSymbolCardView.center.y = start.y + gestureTranslation * sin(signCoeficient * AnimatorFactory.angle)
        }
        
        switch gesture.state {
        case .changed:
            if abs(translationValue) <= 0.4 {
                rotateFront(by: translationValue)
            }
            showHelpLabel(by: translationValue)
        case .ended:
            hideHelpLabels(animated: true)
            
            if abs(translationValue) <= 0.4 {
                moveFrontOnStart()
            } else {
                AnimatorFactory.scaleAndMove(view: backSymbolCardView).startAnimation()
                setupScaleDown()
                moveFront(by: translationValue)
            }
        default:
            break
        }
        
    }
    
    @objc private func panSymbolsView(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            beganPanSambols()
        case .changed:
            let location = gesture.location(in: symbolsStackView)
            changedPanLocation(location)
        default:
            finishedPanSymbols()
        }
        
    }
    
    private func rotateFront(by translationValue: CGFloat) {
        if translationValue > 0 {
            rightRotation.fractionComplete = translationValue
        } else {
            leftRotation.fractionComplete = abs(translationValue)
        }
        
    }
    
    private func moveFrontOnStart() {
        leftRotation.fractionComplete = 0
        rightRotation.fractionComplete = 0
        frontSymbolCardView.center = start
    }
    
    private func moveFront(by translationValue: CGFloat) {
        switch translationValue.sign {
        case .plus:
            moveRight = AnimatorFactory.moveRight(view: frontSymbolCardView)
            moveRight.addCompletion { [weak self] _ in
                guard let self = self else { return }
                self.moveCompletion(with: LearningState.learned)
            }
            moveRight.startAnimation()
        case .minus:
            moveLeft = AnimatorFactory.moveLeft(view: frontSymbolCardView)
            moveLeft.addCompletion { [weak self] _ in
                guard let self = self else { return }
                self.moveCompletion(with: LearningState.unlearned)
            }
            moveLeft.startAnimation()
        }
    }
    
    private func moveCompletion(with state: LearningState) {
        updateCurrentSymbol(state: state)
        
        scaleDownAndMove.startAnimation()
        
        frontSymbolCardView.center = start
        
        state == .learned ? (rightRotation.fractionComplete = 0) : (leftRotation.fractionComplete = 0)
        
        frontSymbolCardView.isHidden = false
    }
    
    private func setupScaleDown() {
        scaleDownAndMove = AnimatorFactory.scaleAndMove(view: backSymbolCardView, to: HomeSettings.kBackCardMinScale)
        scaleDownAndMove.addCompletion({ [weak self] _ in
            guard let self = self else { return }
            self.updateBackSymbol()
        })
    }
    
    private func front (_ state: LearningState) -> (() -> ())  {
        return { [weak self] in
            guard let self = self else { return }
            self.updateCurrentSymbol(state: state)
        }
    }
    
    private func showHelpLabel(by angle: CGFloat) {
        yesAnimation.pauseAnimation()
        noAnimation.pauseAnimation()
        
        yesAnimation.isReversed = false
        noAnimation.isReversed = false
        
        var fraction = abs(angle) / HomeSettings.kMaxMoveCardAngle + 0.25
        
        if fraction >= 1 {
            fraction = 1
        }
        
        if angle > 0 {
            yesAnimation.fractionComplete = fraction
            noAnimation.fractionComplete = 0
        } else {
            noAnimation.fractionComplete = fraction
            yesAnimation.fractionComplete = 0
        }
    }
    
    private func hideHelpLabels(animated: Bool = false) {
        if animated {
            yesAnimation.pausesOnCompletion = true
            noAnimation.pausesOnCompletion = true
            
            yesAnimation.isReversed = true
            yesAnimation.startAnimation()
            
            noAnimation.isReversed = true
            noAnimation.startAnimation()
        } else {
            yesAnimation.fractionComplete = 0
            noAnimation.fractionComplete = 0
        }
    }
    
    private func zoomOutHelpLabels() {
        yesLabel.transform = CGAffineTransform(scaleX: 0, y: 0)
        noLabel.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    private func setupYesAndNoAnimation() {
        yesAnimation = UIViewPropertyAnimator(duration: 0.23, curve: .easeIn) { [weak self] in
            guard let self = self else { return }
            self.yesLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
        yesAnimation.startAnimation()
        yesAnimation.pauseAnimation()
        
        noAnimation = UIViewPropertyAnimator(duration: 0.23, curve: .easeIn) { [weak self] in
            guard let self = self else { return }
            self.noLabel.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
        noAnimation.startAnimation()
        noAnimation.pauseAnimation()
    }
    
    private func stopYesAndNoAnimation() {
        yesAnimation.stopAnimation(false)
        yesAnimation.finishAnimation(at: .start)
        
        noAnimation.stopAnimation(false)
        noAnimation.finishAnimation(at: .start)
    }
    
    private func setupRotationAnimation() {
        leftRotation = AnimatorFactory.rotateLeft(view: frontSymbolCardView)
        leftRotation.startAnimation()
        leftRotation.pauseAnimation()
        
        rightRotation = AnimatorFactory.rotateLRight(view: frontSymbolCardView)
        rightRotation.startAnimation()
        rightRotation.pauseAnimation()
    }
    
    private func stopRotationAnimation() {
        leftRotation.stopAnimation(false)
        rightRotation.stopAnimation(false)
        
        leftRotation.finishAnimation(at: .end)
        rightRotation.finishAnimation(at: .end)
        
        frontSymbolCardView.center = start ?? frontSymbolCardView.center
    }
    private func updateCurrentSymbol(state: LearningState) {
        viewModel.updateSymbolState(state)
        let currentSymbol = symbolsStackView.arrangedSubviews[viewModel.currentSymbolIndex] as! SymbolView
        currentSymbol.isSelected = false
        currentSymbol.learningState = state
        viewModel.updateSelectedIndex(viewModel.nextSymbolIndex)
        setCard(frontSymbolCardView, by: viewModel.currentSymbolIndex)
        let nextSymbol = symbolsStackView.arrangedSubviews[viewModel.currentSymbolIndex] as! SymbolView
        nextSymbol.isSelected = true
    }
    
    private func updateBackSymbol() {
        setCard(backSymbolCardView, by: viewModel.nextSymbolIndex)
    }
    
    private func beganPanSambols() {
        previewLabel.isHidden = false
        testButton.isHidden = true
    }
    
    private func changedPanLocation(_ location: CGPoint) {
        let symbolOnPanIndex = Int(location.x / (symbolsStackView.frame.width / CGFloat(viewModel.symbolCount)))
        if pannedSymbol != symbolOnPanIndex {
            if symbolOnPanIndex < 0 {
                pannedSymbol = 0
            } else if symbolOnPanIndex >= viewModel.symbolCount {
                pannedSymbol = viewModel.symbolCount - 1
            } else {
                pannedSymbol = symbolOnPanIndex
            }

            previewLabel.text = viewModel.symbolCapital(at: pannedSymbol)
            
            let symbolWidth = symbolsStackView.frame.width / CGFloat(viewModel.symbolCount)
            var xShift = CGFloat(pannedSymbol) * symbolWidth - (previewLabel.intrinsicContentSize.width - symbolWidth) / 2
            if xShift + previewLabel.frame.width > view.frame.width {
                xShift = view.frame.width - previewLabel.intrinsicContentSize.width - 1
            } else if xShift < 0 {
                xShift = 1
            }
            
            leadingConstraint.constant = xShift
        }
    }
    
    private func finishedPanSymbols() {
        previewLabel.isHidden = true
        testButton.isHidden = false
        var symbolView = symbolsStackView.arrangedSubviews[viewModel.currentSymbolIndex] as! SymbolView
        symbolView.isSelected = false
        viewModel.updateSelectedIndex(pannedSymbol)
        symbolView = symbolsStackView.arrangedSubviews[viewModel.currentSymbolIndex] as! SymbolView
        setupSymbolCards()
        symbolView.isSelected = true
    }
    
    private func setupSymbols() {
        symbolsStackView.arrangedSubviews.forEach {
            symbolsStackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
        for (index, symbol) in viewModel.normalSymbols.enumerated() {
            let symbolView = SymbolView(with: symbol, state: viewModel.learningState(at: index))
            symbolView.isSelected = index == viewModel.currentSymbolIndex
            symbolsStackView.addArrangedSubview(symbolView)
        }
    }
    
    private func setupSymbolCards() {
        setCard(frontSymbolCardView, by: viewModel.currentSymbolIndex)
        setCard(backSymbolCardView, by: viewModel.nextSymbolIndex)
    }
    
    private func setCard(_ card: SymbolCardView, by index: Int) {
        card.mainLabel?.text = "\(viewModel.symbolCapital(at: index))  \(viewModel.symbolNormal(at: index))"
        card.transliterationLabel?.text = viewModel.symbolTransliteration(at: index)
        card.pronunciationExampleLabel.attributedText = viewModel.symbolEnglishPronunciation(at: index).htmlAttrString(fontSize: 14.0, options: String.htmlOptions)
        card.originalExampleLabel.attributedText = viewModel.originalExample(at: index).htmlAttrString(fontSize: 16.0, options: String.htmlOptions)
        card.translatedExampleLabel?.text = viewModel.translatedExample(at: index)
    }
    
    private func setupNavBarButtons() {
        let languageButtonItem = getBarBattonItem(with: languageButton, action: #selector(changeLanguage(_:)), bgImage: viewModel.languageImage)
        navigationItem.leftBarButtonItem = languageButtonItem
        
        let infoButtonItem = getBarBattonItem(with: infoButton, action: #selector(infoButtonTouched(_:)), bgImage: viewModel.infoImage, height: 16, width: 4)
        let difficaltyItem = getBarBattonItem(with: difficaltyButton, action: #selector(changeOrder(_:)), bgImage: viewModel.difficaltyImage)
        navigationItem.rightBarButtonItems = [infoButtonItem, difficaltyItem]
    }
    
    private func getBarBattonItem(with button: UIButton, action: Selector, bgImage: UIImage?, height: CGFloat? = nil, width: CGFloat? = nil) -> UIBarButtonItem {
        
        activateSizeConstrainsForView(button)
        button.setImage(bgImage, for: .normal)
        button.addInsets(byButtonSize: viewModel.navBarButtonRatioSize,
                         contentHeight: height,
                         contentWidth: width)
        button.addTarget(self, action: action, for: .touchUpInside)
        
        return  UIBarButtonItem(customView: button)
    }
    
    private func activateSizeConstrainsForView(_ view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        let widthConstraint = view.widthAnchor.constraint(equalToConstant: viewModel.navBarButtonRatioSize.width)
        let heightConstraint = view.widthAnchor.constraint(equalToConstant: viewModel.navBarButtonRatioSize.width)
        
        NSLayoutConstraint.activate([widthConstraint, heightConstraint])
    }
    
}
