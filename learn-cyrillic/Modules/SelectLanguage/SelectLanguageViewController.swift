//
//  LanguageListTableViewController.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 2/25/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class SelectLanguageViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let languageCellIdentifier = "LanguageTableViewCell"
    public weak var coordinator: SelectLanguageCoordinator?
    public var viewModel: SelectLanguageViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.subscribe { [weak self] error in
            guard let self = self else { return }
            DispatchQueue.main.async {
                guard error == nil else {
                    self.displayAlert(title: "error".localized, msg: error!.localizedDescription)
                    return
                }
                
                self.tableView.reloadData()
            }
        }
        
        navigationItem.titleView = UILabel.navBarTitleView(title: "select_language".localized)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        coordinator?.didFinishSelecting()
    }
    
}

extension SelectLanguageViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.languageCount
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: languageCellIdentifier, for: indexPath) as! LanguageTableViewCell
        
        cell.languageTitleLabel?.text = viewModel.languageTitle(at: indexPath)
        cell.languageFlagImageView.image = viewModel.languageFlag(at: indexPath)
        cell.languageSubtitleLabel?.text = viewModel.languageSubtitle(at: indexPath)
        cell.isActiveLanguage = viewModel.languageIsActive(at: indexPath)
        
        cell.shadowed()
        
        return cell
    }
}
    
extension SelectLanguageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if  let cell = tableView.cellForRow(at: indexPath) as? LanguageTableViewCell,
            cell.isActiveLanguage {
            viewModel.selectLanguage(at: indexPath)
        }
    }

}
