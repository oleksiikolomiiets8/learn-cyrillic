//
//  SelectLanguageViewModel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

protocol SelectLanguageViewModelProtocol: ViewModel {
    var languageCount: Int { get }
    func languageTitle(at indexPath: IndexPath) -> String
    func languageSubtitle(at indexPath: IndexPath) -> String
    func languageFlag(at indexPath: IndexPath) -> UIImage?
    func languageIsActive(at indexPath: IndexPath) -> Bool
    func selectLanguage(at indexPath: IndexPath)
}

final class SelectLanguageViewModel {
    
    public var selectedLanguage: ((_: LanguageType) -> Void) = { _ in
        print("selectedLanguage not overriden")
    }
    private var updateCallback: ViewModelCallback = nil
    private var languages = LanguageType.allCases
    
    private func language(at indexPath: IndexPath) -> LanguageType {
        return languages[indexPath.row]
    }
}

extension SelectLanguageViewModel: SelectLanguageViewModelProtocol {
    
    var languageCount: Int {
        return languages.count
    }
    
    func subscribe(with updateCallback: ViewModelCallback) {
        self.updateCallback = updateCallback
    }
    
    func languageTitle(at indexPath: IndexPath) -> String {
        return languages[indexPath.row].title
    }
    
    func languageSubtitle(at indexPath: IndexPath) -> String {
        return languages[indexPath.row].subtitle
    }
    
    func languageFlag(at indexPath: IndexPath) -> UIImage? {
        return UIImage(named: languages[indexPath.row].rawValue)
    }
    
    func languageIsActive(at indexPath: IndexPath) -> Bool {
        return languages[indexPath.row].isActive
    }
    
    func selectLanguage(at indexPath: IndexPath) {
        selectedLanguage(language(at: indexPath))
    }
    
}
