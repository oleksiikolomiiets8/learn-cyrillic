//
//  TestCompleteViewController.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/26/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class TestCompleteViewController: UIViewController, Storyboarded {

    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var testCompleteView: TestCompleteView!
    
    public var viewModel: TestCompleteViewModel!
    
    @IBAction func dismiss(_ sender: Any) {
        viewModel.dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        testCompleteView.cyrillicNameLabel?.text = viewModel.testCompleteCardTitle
        testCompleteView.progressValue = viewModel.progressValue
        testCompleteView.setCorrectAnswersTitleLabel(correct: viewModel.correctAnswers, count: viewModel.testCount)
        testCompleteView.flagImageView.image = viewModel.languageFlagImage
        testCompleteView.progressValue = viewModel.progressValue
        testCompleteView.circleProgressView.progress = Float(viewModel.correctAnswers) / Float(viewModel.testCount)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
