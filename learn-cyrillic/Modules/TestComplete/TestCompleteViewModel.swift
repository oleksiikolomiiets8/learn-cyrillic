//
//  TestCompleteViewModel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/26/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation
import UIKit

protocol TestCompleteViewModelProtocol {
    var dismiss: () -> Void { get set }
    var testCompleteCardTitle: String { get }
    var progressValue: Int { get }
    var correctAnswers: Int { get }
    var testCount: Int { get }
    var languageFlagImage: UIImage? { get }
}

class TestCompleteViewModel {
    
    weak static var lastTest: TestCompleteViewModel!
    
    public var language: LanguageType!
    public var dismiss: () -> Void = { print("dismiss not overriden") }
    public var testSymbols: [Symbol: [VariantProtocol]]!
}

extension TestCompleteViewModel: TestCompleteViewModelProtocol {

    var testCompleteCardTitle: String {
        return "\(language.title)" + " " + "Cyrillic".localized
    }
    
    var progressValue: Int {
        return testCount == 0 ? 0 : Int(100 * correctAnswers / testCount)
    }
    
    var correctAnswers: Int {
        return Array(testSymbols.filter({ $0.key.state == .learned })).count
    }
    
    var testCount: Int {
        return testSymbols.count
    }
    
    var languageFlagImage: UIImage? {
        return UIImage(named: language.rawValue)
    }
}
