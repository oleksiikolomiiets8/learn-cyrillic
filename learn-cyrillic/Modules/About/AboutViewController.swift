//
//  AboutViewController.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, Storyboarded {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainAppAboutView: UIView!
    @IBOutlet weak var versionLabel: UILabel!
    
    public var viewModel: AboutViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainAppAboutView.round(radius: 16)
        versionLabel.text = viewModel.version
    }

}

extension AboutViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(at: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutViewCell", for: indexPath) as! AboutViewCellTableViewCell
        cell.titleLabel.text = viewModel.rowTitle(for: indexPath)
        cell.valueLabel.attributedText = viewModel.rowValue(for: indexPath)
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sectionTitle(for: section)
    }
    
    
}

extension AboutViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UILabel.sectionHeaderTitleView(title: viewModel.sectionTitle(for: section))
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.headerHeight
    }
}
