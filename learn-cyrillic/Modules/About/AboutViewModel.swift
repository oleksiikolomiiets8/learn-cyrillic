//
//  AboutViewModel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation
import UIKit

enum AboutSectionType: Int, CaseIterable {
    case development = 0
    case experts
    case source
    
    var title: String {
        switch self {
        case .development:
            return "development".localized
        case .experts:
            return "experts".localized
        case .source:
            return "source".localized
        }
    }
}

protocol AboutViewModelProtocol: ViewModel {
    var numberOfSections: Int { get }
    var headerHeight: CGFloat { get }
    var version: String { get }
    func numberOfRows(at section: Int) -> Int
    func sectionTitle(for section: Int) -> String?
    func rowTitle(for indexPath: IndexPath) -> String?
    func rowValue(for indexPath: IndexPath) -> NSAttributedString?
}

final class AboutViewModel {
    
    func subscribe(with updateCallback: ViewModelCallback) {
        
    }
}

extension AboutViewModel: AboutViewModelProtocol {
    
    var numberOfSections: Int {
        return AboutSectionType.allCases.count
    }
    
    var headerHeight: CGFloat {
        return 34
    }
    
    var version: String {
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        return String(format: "app_version".localized, versionNumber)
    }
    
    func numberOfRows(at section: Int) -> Int {
        guard let section = AboutSectionType(rawValue: section) else { return 0 }
        
        switch section {
        case .development:
            return Development.allCases.count
        case .experts:
            return Experts.allCases.count
        case .source:
            return Source.allCases.count
        }
    }
    
    func sectionTitle(for section: Int) -> String? {
        return AboutSectionType(rawValue: section)?.title
    }
    
    func rowTitle(for indexPath: IndexPath) -> String? {
        guard let section = AboutSectionType(rawValue: indexPath.section) else { return nil }
        
        switch section {
        case .development:
            return Development.allCases[indexPath.row].title
        case .experts:
            return Experts.allCases[indexPath.row].title
        case .source:
            return Source.allCases[indexPath.row].title
        }
    }
    
    func rowValue(for indexPath: IndexPath) -> NSAttributedString? {
        guard let section = AboutSectionType(rawValue: indexPath.section) else { return nil }
        
        switch section {
        case .development:
            return Development.allCases[indexPath.row].value
        case .experts:
            return Experts.allCases[indexPath.row].value
        case .source:
            return Source.allCases[indexPath.row].value
        }
    }
}
