//
//  Symbol.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 4/8/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

protocol SymbolProtocol {
    var id: Int { get }
    var cyrillicCaps: String { get }
    var cyrillicNormal: String { get }
    var transliteration: String { get }
    var name: String { get }
    var pronunciation: String { get }
    var englishPronunciation: String { get }
    var difficulty: Int { get set }
    var examples: [SymbolExample] { get }
    var state: LearningState { get set }
}

protocol SymbolExampleProtocol {
    var cyrillic: String { get }
    var englishTranslation: String { get }
}


struct Symbol: SymbolProtocol, Codable, Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Symbol, rhs: Symbol) -> Bool {
        return lhs.id == rhs.id
    }
    
    let id: Int
    let cyrillicCaps: String
    let cyrillicNormal: String
    let transliteration: String
    let name: String
    let pronunciation: String
    let englishPronunciation: String
    var difficulty: Int
    let examples: [SymbolExample]
    
    var state: LearningState
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        cyrillicCaps = try container.decode(String.self, forKey: .cyrillicCaps)
        cyrillicNormal = try container.decode(String.self, forKey: .cyrillicNormal)
        transliteration = try container.decode(String.self, forKey: .transliteration)
        name = try container.decode(String.self, forKey: .name)
        pronunciation = try container.decode(String.self, forKey: .pronunciation)
        englishPronunciation = try container.decode(String.self, forKey: .englishPronunciation)
        difficulty = try container.decode(Int.self, forKey: .difficulty)
        examples = try container.decode([SymbolExample].self, forKey: .examples)
    
        do {
            state = try container.decode(LearningState.self, forKey: .state)
        } catch {
            state = .undisclosed
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case cyrillicCaps = "cyrillic_caps"
        case cyrillicNormal = "cyrillic_normal"
        case transliteration
        case name
        case pronunciation
        case englishPronunciation = "pronunciation_in_english_word"
        case difficulty
        case examples
        case state
    }

}

struct SymbolExample:  Codable, SymbolExampleProtocol {
    let cyrillic: String
    let englishTranslation: String
    
    private enum CodingKeys: String, CodingKey {
        case cyrillic
        case englishTranslation = "english_translation"
    }
}
