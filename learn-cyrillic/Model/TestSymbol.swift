//
//  TestSymbol.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 4/10/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

struct TestSymbol {
    let symbol: String
    let variants: [VariantProtocol]
}
