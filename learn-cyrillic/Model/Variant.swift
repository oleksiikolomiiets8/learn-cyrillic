//
//  Variant.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 4/10/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

protocol VariantProtocol {
    var value: String { get }
    var isCorrect: Bool { get }
}

struct Variant: VariantProtocol {
    let value: String
    let isCorrect: Bool
}
