//
//  File.swift
//  learn-cyrillic
//
//  Created by Olexander Markov on 1/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

struct LanguageItem: Codable {
    let language: Language
}

struct Language: Codable, Equatable {
    var languageCode: String
    var symbols: [Symbol]
    
    enum CodingKeys: String, CodingKey {
        case languageCode = "language_code"
        case symbols
    }
    
    static public func instance(from data: Data) -> Language? {
        return try? JSONDecoder().decode(Language.self, from: data)
    }
    
    public var data: Data {
        return try! JSONEncoder().encode(self) as Data
    }
}


