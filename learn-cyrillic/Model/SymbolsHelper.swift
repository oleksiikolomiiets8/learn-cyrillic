//
//  SymbolsLoader.swift
//  learn-cyrillic
//
//  Created by Olexander Markov on 1/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

enum SymbolOrder {
    case ascending
    case difficulty
    
    var buttonTitle: String {
        switch self {
        case .ascending:
            return "alphabet"
        case .difficulty:
            return "difficulty"
        }
    }
}

fileprivate class SymbolsHelperSetting {
    static let kLearnedSymbolForTestCount = 2
    static let kUnlearnedSymbolForTestCount = 2
    static let kSymbolForTestCount = 5
}

class SymbolsHelper {
    
    private var dictionaryOfLanguages = [LanguageType: Language] () {
        willSet {
            SymbolsHelper.dictionary = newValue
        }
    }
    
    static private var dictionary = [LanguageType: Language]()
    
    init() {
        let savedLanguages = LanguageSaver.savedLanguages
        if savedLanguages.isEmpty {
            LanguageType.allCases.forEach {
                dictionaryOfLanguages[$0] = getLanguage(forCode: $0)
            }
            LanguageSaver.saveLanguages(dictionaryOfLanguages)
        } else {
            dictionaryOfLanguages = savedLanguages
        }
        SymbolsHelper.dictionary = dictionaryOfLanguages
    }
    
    private func getLanguage(forCode code: LanguageType) -> Language? {
        if let path = Bundle.main.path(forResource: code.rawValue, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let languageItem = try JSONDecoder().decode(LanguageItem.self , from: data)
                return languageItem.language
            } catch let error {
                print(error)
            }
        }
        return nil
    }
    
    func getSymcolsBylanguageCode(atCode code: LanguageType, orderBy: SymbolOrder) -> [Symbol]? {
        if let language = dictionaryOfLanguages[code] {
            switch orderBy {
            case .ascending:
                return language.symbols
            case .difficulty:
                return language.symbols.sorted { $0.difficulty > $1.difficulty }
            }
        }
        return nil
    }
    
    func getRandomSymbols(atCode code: LanguageType) -> [Symbol: [Variant]]? {
        var symbols = [Symbol]()
        
        let learned = Array(getSymbols(for: code, by: .learned).shuffled().prefix(SymbolsHelperSetting.kLearnedSymbolForTestCount))
        symbols += learned
        
        let allUnlearned = getSymbols(for: code, by: .unlearned).shuffled()
        let unlearned = Array(allUnlearned.prefix(SymbolsHelperSetting.kUnlearnedSymbolForTestCount))
        symbols += unlearned
        
        let restSymbolsCount = SymbolsHelperSetting.kSymbolForTestCount - symbols.count
        var undisclosed = Array(getSymbols(for: code, by: .undisclosed).shuffled().prefix(restSymbolsCount))
        if undisclosed.isEmpty {
            undisclosed = Array(allUnlearned.suffix(restSymbolsCount))
        }
        symbols += undisclosed
        
        var symbolsWithVariants = [Symbol: [Variant]]()
        Array(symbols.shuffled()).forEach { symbol in
            symbolsWithVariants[symbol] = getRandomVariants(atCode: code, for: symbol)
        }
        return symbolsWithVariants
    }
    
    func getRandomVariants(atCode code: LanguageType, for symbol: Symbol) -> [Variant]? {
        guard let language = dictionaryOfLanguages[code],
            language.symbols.contains(symbol) else { return nil }
        
        var variants = [Variant(value: getPronunciationSymbol(symbol.pronunciation), isCorrect: true)]
        
        Array(language.symbols.filter { $0 != symbol }.shuffled().prefix(2)).forEach { wrongVariantSymbol in
            variants.append(Variant(value: getPronunciationSymbol(wrongVariantSymbol.pronunciation), isCorrect: false))
        }
        
        return variants.shuffled()
    }
    
    func getPronunciationSymbol(_ pronunciation: String) -> String {
        return String(pronunciation.replacingOccurrences(of: "/", with: "", options: .literal, range: nil).first ?? Character.init("_")).uppercased()
    }
    
    func setLearningState(_ state: LearningState, for languageType: LanguageType, _ currentSymbol: Symbol) {
        guard let language = dictionaryOfLanguages[languageType],
            let index = language.symbols.firstIndex(of: currentSymbol) else { return }
        
        dictionaryOfLanguages[languageType]!.symbols[index].state = state
        dictionaryOfLanguages[languageType]!.symbols[index].difficulty = state.difficulty ?? language.symbols[index].difficulty
        
        LanguageSaver.updateLanguages(dictionaryOfLanguages)
    }
    
    func getLearnedCount(for languageType: LanguageType) -> Int {
        return getSymbols(for: languageType, by: .learned).count
    }
    
    private func getSymbols(for languageType: LanguageType, by state: LearningState) -> [Symbol] {
        guard let language = dictionaryOfLanguages[languageType] else { return [] }
        return language.symbols.filter{ $0.state == state }
    }
    
    static public func isLanguageTested(by type: LanguageType) -> Bool {
        guard let symbols = dictionary[type]?.symbols else { return false }
        
        for symbol in symbols {
            if symbol.state != .undisclosed {
                return true
            }
        }
        return false
    }
    
    static public func learnedSymbolsCount(by type: LanguageType) -> Int {
        return dictionary[type]?.symbols.filter { $0.state == .learned }.count ?? 0
    }
}
