//
//  UILabel+NavBarTitle.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/5/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

extension UILabel {
    static func navBarTitleView(title: String) -> UILabel {
        let label = UILabel()
        
        label.textColor = UIColor.dark
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.text = title
        
        return label
    }
    
    static func sectionHeaderTitleView(title: String?) -> UILabel {
        let label = UILabel()
        
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .left
        label.text = title
        
        return label
    }
}
