//
//  UIView+Rounding.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/6/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

extension UIView {
    
    func rounded() {
        layer.cornerRadius = min(frame.width, frame.height) / 2
        clipsToBounds = true
    }
    
    func round(radius: CGFloat) {
        if radius <= (min(frame.width, frame.height) / 2) {
            layer.cornerRadius = radius
        }
    }
    
}
