//
//  UIView+Shadow.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/5/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

extension UIView {
    
    func shadowed() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
        layer.shadowRadius = 10
    }
    
}
