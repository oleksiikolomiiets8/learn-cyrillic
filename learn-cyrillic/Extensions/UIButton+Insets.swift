//
//  UIButton+Insets.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 4/17/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func addInsets(byButtonSize: CGSize, contentHeight: CGFloat?, contentWidth: CGFloat?) {
        var leftRightOffset: CGFloat = 0
        var topBottomOffset: CGFloat = 0
        
        if let width = contentWidth,
            width < byButtonSize.width {
            
            leftRightOffset = (byButtonSize.width - width) / 2
        }
        if let height = contentHeight,
            height < byButtonSize.height {
            
            topBottomOffset = (byButtonSize.height - height) / 2
        }
        
        imageEdgeInsets = UIEdgeInsets(top: topBottomOffset, left: leftRightOffset, bottom: topBottomOffset, right: leftRightOffset)
    }
}
