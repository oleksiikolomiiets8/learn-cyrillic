//
//  String+AttributedString.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

extension String {
    static let htmlOptions: [NSAttributedString.DocumentReadingOptionKey : Any] = [.documentType: NSAttributedString.DocumentType.html,
                                                                               .characterEncoding: String.Encoding.utf8.rawValue]
    
    func htmlAttrString(fontSize: CGFloat, options: [NSAttributedString.DocumentReadingOptionKey : Any]) -> NSAttributedString? {
        
        let font = UIFont.preferredFont(forTextStyle: .body).withSize(fontSize).fontName
        let newString = self.appending(String(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", font, fontSize))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        
        let stringData = Data(newString.utf8)
        
        if let attributedString = try? NSAttributedString(data: stringData, options: options, documentAttributes: nil) {
            return attributedString
        }
        
        return nil
    }
    
    func attributedString(color: UIColor) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: [.strokeColor : color])
    }
    
    var underlinedAttrString: NSAttributedString {
        let textRange = NSMakeRange(0, self.count)
        let attributedText = NSMutableAttributedString(string: self)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        
        return attributedText
    }
    
}
