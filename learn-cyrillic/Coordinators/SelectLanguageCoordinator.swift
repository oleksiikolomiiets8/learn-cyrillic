//
//  ArticleCoordinator.swift
//  Arch List PLay
//
//  Created by Andy Bell on 26.02.19.
//  Copyright © 2019 snapp mobile. All rights reserved.
//

import UIKit

final class SelectLanguageCoordinator: Coordinator {

    weak var parentCoordinator: MainCoordinator?

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = SelectLanguageViewController.instantiate()
        let vm = SelectLanguageViewModel()
        vm.selectedLanguage = { [weak self] language in
            self?.selectLanguage(language)
        }
        vc.viewModel = vm
        vc.coordinator = self
        
        navigationController.pushViewController(vc, animated: true)
    }
    
    func selectLanguage(_ language: LanguageType) {
        parentCoordinator?.changeLanguage(to: language)
        didFinishSelecting()
    }
    
    func didFinishSelecting() {
        parentCoordinator?.childDidFinish(self)
        navigationController.popToRootViewController(animated: true)
    }
}
