//
//  TestCoordinator.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/5/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit
import Foundation

class TestCoordinator: Coordinator {
    
    weak var parentCoordinator: MainCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    private let testingLanguage: LanguageType
    private let symbolsHelper: SymbolsHelper
    
    init(navigationController: UINavigationController, language: LanguageType, symbolsHelper: SymbolsHelper) {
        self.navigationController = navigationController
        self.symbolsHelper = symbolsHelper
        testingLanguage = language
    }
    
    func start() {
        let vc = TestViewController.instantiate()
        let vm = TestViewModel(with: symbolsHelper, testingLanguage)
        vm.dismiss = { [weak self] in
            self?.dismiss()
        }
        vm.testComplete = { [weak self] testedSymbols in
            guard let self = self else { return }
            
            let child = TestCompleteCoordinator(navigationController: self.navigationController,
                                                language: self.testingLanguage,
                                                testedSymbols: testedSymbols)
            MainCoordinator.lastTestViewModel = TestCompleteViewModel()
            MainCoordinator.lastTestViewModel.language = self.testingLanguage
            MainCoordinator.lastTestViewModel.testSymbols = testedSymbols
            child.parentCoordinator = self
            self.presentChild(child)
        }
        vc.viewModel = vm
        
        navigationController.pushViewController(vc, animated: true)
    }
    
    func dismiss() {
        navigationController.popToRootViewController(animated: true)
    }
    
    func presentChild(_ childV: Coordinator) {
        childCoordinators.append(childV)
        childV.start()
    }
    
//    func showTestComplete() {
//        navigationController.present(<#T##viewControllerToPresent: UIViewController##UIViewController#>, animated: <#T##Bool#>, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
//    }
    
}
