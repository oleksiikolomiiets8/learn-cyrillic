//
//  TestCompleteCoordinator.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/26/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation
import UIKit

class TestCompleteCoordinator: Coordinator {
    
    public var parentCoordinator: Coordinator?
    public var childCoordinators = [Coordinator]()
    public var navigationController: UINavigationController
    
    private let language: LanguageType
    private let testedSymbols: [Symbol: [VariantProtocol]]
    
    init(navigationController: UINavigationController, language: LanguageType, testedSymbols: [Symbol: [VariantProtocol]]) {
        self.navigationController = navigationController
        
        self.language = language
        self.testedSymbols = testedSymbols
    }
    
    public func start() {
        let testCompleteVC = TestCompleteViewController.instantiate()
        let testCompleteVM = TestCompleteViewModel()
        testCompleteVM.language = language
        testCompleteVM.testSymbols = testedSymbols
        testCompleteVM.dismiss = { [weak self] in
            self?.backToRoot()
        }
        testCompleteVC.viewModel = testCompleteVM
        navigationController.pushViewController(testCompleteVC, animated: true)
    }
    
    private func backToRoot() {
        navigationController.popToRootViewController(animated: true)
    }
}
