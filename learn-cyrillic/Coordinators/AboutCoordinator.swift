//
//  AboutCoordinator.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit
import Foundation

final class AboutCoordinator: Coordinator {
    
    public weak var parentCoordinator: MainCoordinator?
    public var childCoordinators = [Coordinator]()
    public var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let aboutVC = AboutViewController.instantiate()
        let aboutVM = AboutViewModel()
        aboutVC.viewModel = aboutVM
        navigationController.pushViewController(aboutVC, animated: true)
    }
    
    
}
