//
//  MainCoordinator.swift
//  Arch List PLay
//
//  Created by Andy Bell on 25.02.19.
//  Copyright © 2019 snapp mobile. All rights reserved.
//

import Foundation
import UIKit

final class MainCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {
    
    public var parentCoordinator: MainCoordinator?
    public var childCoordinators = [Coordinator]()
    public var navigationController: UINavigationController
    public let symbolsHelper = SymbolsHelper()
    static var lastTestViewModel: TestCompleteViewModel!
    
    public weak var homeVC: HomeViewController!
    private var language: LanguageType
    
    private var selectLanguage: () -> () {
        return { [weak self] in
            guard let self = self else { return }
            
            let child = SelectLanguageCoordinator(navigationController: self.navigationController)
            child.parentCoordinator = self
            self.displayChild(child)
        }
    }
    private var testYourKnowledge: (LanguageType) -> () {
        return { [weak self] language in
            guard let self = self else { return }
            
            let child = TestCoordinator(navigationController: self.navigationController,
                                        language: language,
                                        symbolsHelper: self.symbolsHelper)
            child.parentCoordinator = self
            self.displayChild(child)
        }
    }
    private var showAbout: () -> () {
        return { [weak self] in
            guard let self = self else { return }
            
            let child = AboutCoordinator(navigationController: self.navigationController)
            child.parentCoordinator = self
            self.displayChild(child)
        }
    }
    private var lastCompletedTest: () -> () {
        return { [weak self] in
            guard let self = self else { return }
            guard MainCoordinator.lastTestViewModel != nil else {
                self.navigationController.displayAlert(title: "last_alert_title".localized,
                                                       msg: "last_alert_msg".localized)
                return
            }
            
            let child = TestCompleteCoordinator(navigationController: self.navigationController,
                                                language: MainCoordinator.lastTestViewModel.language,
                                                testedSymbols: MainCoordinator.lastTestViewModel.testSymbols)
            child.parentCoordinator = self
            self.displayChild(child)
        }
    }

    init(navigationController: UINavigationController, language: LanguageType) {
        self.navigationController = navigationController
        self.language = language
    }

    func start() {
        navigationController.delegate = self
        
        homeVC = HomeViewController.instantiate()
        let homeVM = HomeViewModel(with: symbolsHelper, language)
        homeVM.selectLanguage = selectLanguage
        homeVM.testYourKnowledge = testYourKnowledge
        homeVM.showInfo = showAbout
        homeVM.lastCompletedTest = lastCompletedTest
        homeVC.viewModel = homeVM

        navigationController.pushViewController(homeVC, animated: false)
    }
    
    func changeLanguage(to language: LanguageType) {
        homeVC.viewModel.changeLanguage(to: language)
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }

    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {

        guard let fromVC = navigationController.transitionCoordinator?.viewController(forKey: .from),
            navigationController.viewControllers.contains(fromVC) else {
                return
        }
        if let selectLanguageVC = fromVC as? SelectLanguageViewController {
            childDidFinish(selectLanguageVC.coordinator)
        }
    }
    
    func displayChild(_ childV: Coordinator) {
        childCoordinators.append(childV)
        childV.start()
    }
    
    

}
