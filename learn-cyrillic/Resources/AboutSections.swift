//
//  Development.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

enum Development: String, CaseIterable {
    case developer
    case qa
    
    var defaultName: String {
        return "Juhani Lehtimaeki"
    }
    
    var title: String {
        return rawValue.capitalizingFirstLetter()
    }
    
    var value : NSAttributedString {
        //TODO: put valid values
        switch self {
        case .developer:
            return defaultName.underlinedAttrString
        case .qa:
            return defaultName.underlinedAttrString
        }
    }
}

enum Experts: String, CaseIterable {
    case ukrainian
    case macedonian
    
    var defaultName: String {
        return "Juhani Lehtimaeki"
    }
    
    var title: String {
        return rawValue.capitalizingFirstLetter()
    }
    
    var value : NSAttributedString {
        //TODO: put valid values
        switch self {
        case .ukrainian:
            return defaultName.underlinedAttrString
        case .macedonian:
            return defaultName.underlinedAttrString
        }
    }
}

enum Source: String, CaseIterable {
    case link = "Square Otto"
    
    var defaultName: String {
        return "square.github.io/otto/"
    }
    
    var title: String {
        return rawValue.capitalizingFirstLetter()
    }
    
    var value : NSAttributedString {
        //TODO: put valid values
        return defaultName.underlinedAttrString
    }
}
