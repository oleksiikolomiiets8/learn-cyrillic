//
//  SymbolCardView.swift
//  learn-cyrillic
//
//  Created by Olexander Markov on 1/15/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//
import UIKit

class SymbolCardView: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var transliterationLabel: UILabel!
    @IBOutlet weak var pronunciationExampleLabel: UILabel!
    @IBOutlet weak var originalExampleLabel: UILabel!
    @IBOutlet weak var translatedExampleLabel: UILabel!
    @IBOutlet weak var exampleTitleLabel: UILabel!
    @IBOutlet weak var prononciationTitleLable: UILabel!
    @IBOutlet var testVariantButtons: [AnswerButton]!
    @IBOutlet weak var flagImageView: UIImageView!
    
    @IBOutlet weak var testStackView: UIStackView!
    
    @IBOutlet weak var homeStackView: UIStackView!
    
    @IBOutlet weak var separatorLineView: UIView!
    
    @IBAction func shareButtonTouched(_ sender: Any) {
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        
        fromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        contentView.round(radius: 16)
        shadowed()
        
        
    }
    
    
    
}
