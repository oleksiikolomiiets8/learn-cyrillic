//
//  AboutViewCellTableViewCell.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/14/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class AboutViewCellTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
}
