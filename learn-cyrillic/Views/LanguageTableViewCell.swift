//
//  LanguageTableViewCell.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 2/25/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {
    @IBOutlet weak var languageFlagImageView: UIImageView!
    @IBOutlet weak var languageTitleLabel: UILabel!
    @IBOutlet weak var languageInfoView: UIView!
    @IBOutlet weak var languageSubtitleLabel: UILabel!
    
    var isActiveLanguage: Bool!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        languageInfoView.backgroundColor = .white
        languageInfoView.layer.cornerRadius = 16.0
        languageInfoView.alpha = isActiveLanguage ? 1.0 : 0.5
        
    }
    
}
