//
//  TestView.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/19/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class TestView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var separatorLineView: UIView!
    @IBOutlet var answerButtonCollection: [AnswerButton]!
    
    @IBAction func selectAnswerButton(_ sender: AnswerButton) {
        answerButtonCollection.forEach { $0.isSelected = sender == $0 }
        selectDelegate?.selectSymbol(sender)
    }
    
    public var selectDelegate: TestSymbolSelectDelegate?
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        
        fromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        contentView.round(radius: 16)
        shadowed()
        
        
    }
    
    
}
