//
//  SymbolView.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/3/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

protocol SymbolViewProtocol {
    var isSelected: Bool { get set }
    var learningState: LearningState { get set }
}

fileprivate class ViewConfig {
    static let kLearndSymbolBackgroundColor: UIColor    = .slimeGreen
    static let kUnlearnedSymbolBackgroundColor: UIColor = .ruby
    static let kDefaultSymbolBackgroundColor: UIColor   = .iceBlue
    
    static let kDefaultSymbolTextColor: UIColor         = .blueGrey
    static let kHighlightedSymbolTextColor: UIColor     = .white
}

enum LearningState: Int, Codable {
    case learned = 1
    case unlearned = -1
    case undisclosed = 0
    
    init(_ isCorrect: Bool) {
        self = isCorrect ? .learned : .unlearned
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .learned:
            return ViewConfig.kLearndSymbolBackgroundColor
        case .unlearned:
            return ViewConfig.kUnlearnedSymbolBackgroundColor
        case .undisclosed:
            return ViewConfig.kDefaultSymbolBackgroundColor
        }
    }
    
    var characterColor: UIColor {
        switch self {
        case .learned, .unlearned:
            return ViewConfig.kHighlightedSymbolTextColor
        case .undisclosed:
            return ViewConfig.kDefaultSymbolTextColor
        }
    }
    
    var difficulty: Int? {
        switch self {
        case .learned: return 0
        case .unlearned: return 4
        case .undisclosed: return nil
        }
    }
}

class SymbolView: UIView, UIGestureRecognizerDelegate, SymbolViewProtocol {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var symbolViewTopContraint: NSLayoutConstraint!
    
    public var learningState: LearningState = .undisclosed {
        willSet {
            symbolLabel.backgroundColor = newValue.backgroundColor
            symbolLabel.textColor = newValue.characterColor
        }
    }
    public var isSelected: Bool = false {
        willSet {
            symbolViewTopContraint.priority = newValue ? .defaultHigh : .defaultLow
        }
    }
    
    init(with symbol: String, state: LearningState) {
        super.init(frame: .zero)
        
        fromNib()
        symbolLabel.text = symbol
        learningState = state
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        
        fromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        symbolViewTopContraint.priority = isSelected ? .defaultHigh : .defaultLow
        
        symbolLabel.backgroundColor = learningState.backgroundColor
        symbolLabel.textColor = learningState.characterColor
        symbolLabel.layer.masksToBounds = true
        symbolLabel.layer.cornerRadius = contentView.frame.width / 2

    }
}
