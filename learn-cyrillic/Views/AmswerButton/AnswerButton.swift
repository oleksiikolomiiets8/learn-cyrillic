//
//  AnswerButton.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/7/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

protocol Answerable {
    var isAswered: Bool { get set }
}

class AnswerButton: UIButton {
    
    @IBOutlet weak var symbolLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if !isCorrect {
                switch isSelected {
                case true:
                    symbolLabel.backgroundColor = UIColor.grey4
                case false:
                    symbolLabel.backgroundColor = .clear
                }
            }
        }
    }
    
    public var isAnswered: Bool = false {
        didSet {
            symbolLabel.font = UIFont.systemFont(ofSize: (isAnswered && isCorrect) ? 40 : 20, weight: .bold)
        }
    }
    
    private var isCorrect: Bool!
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        return self.loadFromNibIfEmbeddedInDifferentNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        symbolLabel.rounded()
    }
    
    public func setupSymbolLabel(with variant: VariantProtocol) {
        symbolLabel.text = variant.value
        isCorrect = variant.isCorrect
    }
    
    public func isVariantCorrect() -> Bool {
        return isCorrect
    }
}
