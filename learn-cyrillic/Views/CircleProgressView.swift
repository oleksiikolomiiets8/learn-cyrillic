//
//  CircleProgressView.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/11/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class CircleProgressView: UIView {
    var bgPath: UIBezierPath!
    var shapeLayer: CAShapeLayer!
    var progressLayer: CAShapeLayer!
    
    var progress: Float = 0 {
        willSet {
            progressLayer.strokeEnd = CGFloat(newValue)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        bgPath = UIBezierPath()
        simpleShape()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        bgPath = UIBezierPath()
        simpleShape()
    }
    
    private func createCirclePath() {
        let x = frame.width / 2
        let y = frame.height / 2
        let center = CGPoint(x: x, y: y)
        
        bgPath.addArc(withCenter: center, radius: x, startAngle: CGFloat(-1.57), endAngle: CGFloat(4.71), clockwise: true)
        bgPath.close()
    }
    
    func simpleShape() {
        createCirclePath()
        shapeLayer = CAShapeLayer()
        shapeLayer.path = bgPath.cgPath
        shapeLayer.lineWidth = 15
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor(red: 237/255, green: 239/255, blue: 241/255, alpha: 1.0).cgColor
        
        progressLayer = CAShapeLayer()
        progressLayer.path = bgPath.cgPath
        progressLayer.lineWidth = 15
        progressLayer.lineCap = .round
        progressLayer.fillColor = nil
        progressLayer.strokeColor = UIColor.slimeGreen.cgColor
        progressLayer.strokeEnd = 0.0
        
        layer.addSublayer(shapeLayer)
        layer.addSublayer(progressLayer)
    }
}
