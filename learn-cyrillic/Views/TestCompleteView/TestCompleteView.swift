//
//  TestCompleteView.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/18/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class TestCompleteView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var cyrillicNameLabel: UILabel!
    @IBOutlet weak var percentOfLearnedSymbolsLabel: UILabel!
    @IBOutlet weak var correctAnswersTitleLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var circleProgressView: CircleProgressView!
    @IBOutlet weak var flagImageView: UIImageView!
    
    public var progressValue: Int? {
        didSet {
            setPercent()
        }
    }
    
    static var lastTestCompleteView: TestCompleteView?
    
    override var isHidden: Bool {
        didSet {
            if oldValue {
                TestCompleteView.lastTestCompleteView = self
            }
        }
    }
    
    @IBAction func share(_ sender: UIButton) {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        
        fromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        contentView.round(radius: 16)
        shadowed()
        shareButton.rounded()
        setPercent()
    }
    
    public func setCorrectAnswersTitleLabel(correct: Int, count: Int) {
        let str = String(format: "You’ve got %i of %i correct answers", arguments: [correct, count])
        correctAnswersTitleLabel.text = str
    }
    
    public func setPercent() {
        let attributedString = NSMutableAttributedString(string: "\(progressValue ?? 0)%", attributes: [
            .font: UIFont.systemFont(ofSize: 40.0, weight: .bold),
            .foregroundColor: UIColor(red: 151.0 / 255.0, green: 213.0 / 255.0, blue: 0.0, alpha: 1.0),
            .kern: 0.25
            ])
        percentOfLearnedSymbolsLabel.attributedText = attributedString
    }
    
}
