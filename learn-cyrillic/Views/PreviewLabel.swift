//
//  PreviewLabel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/12/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

class PreviewLabel: UILabel {
    
    let inset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: inset))
        
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.dark.cgColor
        round(radius: 12)
    }
    
    override var intrinsicContentSize: CGSize {
        var intrinsicContentSize = super.intrinsicContentSize
        intrinsicContentSize.width += inset.left + inset.right
        intrinsicContentSize.height += inset.top + inset.bottom
        return intrinsicContentSize
    }

}
