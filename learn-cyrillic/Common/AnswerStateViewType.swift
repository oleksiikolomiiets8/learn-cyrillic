//
//  AnswerType.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/6/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

enum AnswerStateViewType: String {
    case positive
    case negative
    case pending
    
    init?(_ state: Bool?) {
        guard let state = state else { return nil }
        
        switch state {
        case true:
            self = .positive
        case false:
            self = .negative
        }
    }
    
    var backgroundColor: UIColor? {
        let backgroundColorAlpha: CGFloat = 0.25
        switch self {
        case .positive:
            return UIColor.slimeGreen.withAlphaComponent(backgroundColorAlpha)
        case .negative:
            return UIColor.ruby.withAlphaComponent(backgroundColorAlpha)
        case .pending:
            return nil
        }
    }
    
    var borderColor: CGColor? {
        return UIColor.white.withAlphaComponent(0.5).cgColor
    }
    
    var image: UIImage? {
        switch self {
        case .positive, .negative:
            return UIImage(named: rawValue)
        case .pending:
            return nil
        }
    }
    
    var title: String? {
        switch self {
        case .positive:
            return "Correct!".localized
        case .negative:
            return "Nope".localized
        case .pending:
            return nil
        }
    }
    
    var titleTextColor: UIColor? {
        switch self {
        case .positive:
            return .slimeGreen
        case .negative:
            return .ruby
        case .pending:
            return nil
        }
    }
}
