//
//  LanguageSaver.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 4/10/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

protocol LanguageSaverProtocol {
    static var savedLanguages: [LanguageType: Language] { get }
    static func saveLanguages(_ languages: [LanguageType : Language])
    static func updateLanguages(_ languages: [LanguageType : Language])
}

class LanguageSaver: LanguageSaverProtocol {
    
    private static let kUserDefaultsKey = "SavedLanguages"
    
    static var savedLanguages: [LanguageType : Language] {
        let savedData = UserDefaults.standard.object(forKey: kUserDefaultsKey) as? [String: Data] ?? [String: Data]()
        var resulDictionary = [LanguageType : Language]()
        savedData.forEach { (typeData, languageData) in
            if let key = LanguageType(rawValue: typeData),
                let value = Language.instance(from: languageData) {
                resulDictionary[key] = value
            }
        }
        return resulDictionary
    }
    
    static func saveLanguages(_ languages: [LanguageType : Language]) {
        if languages != savedLanguages {
            updateLanguages(languages)
        }
    }
    
    static func updateLanguages(_ languages: [LanguageType : Language]) {
        var dataDictionary = [String: Data]()
        
        languages.forEach { (type, language) in
            dataDictionary[type.rawValue] = language.data
        }
        UserDefaults.standard.set(dataDictionary, forKey: kUserDefaultsKey)
    }
    
    static func deketAll() {
        UserDefaults.standard.set(nil, forKey: kUserDefaultsKey)
    }
}
