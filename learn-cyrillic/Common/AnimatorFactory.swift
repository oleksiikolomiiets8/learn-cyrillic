//
//  AnimatorFactory.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/29/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation
import UIKit

class AnimatorFactory {
    
    public static let angle: CGFloat = .pi/45
    
    @discardableResult
    static func scaleAndMove(view: UIView, to scale: CGFloat = 1.0) -> UIViewPropertyAnimator {
        let lengthBetweenViews = view.frame.width * CGFloat(1 - scale) / 4
        let yShift = -(lengthBetweenViews + view.frame.height * CGFloat(1 - scale) / 2 ) / scale
        
        let scaledTransform = CGAffineTransform(scaleX: scale, y: scale)
        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0, y: yShift)
        
        return UIViewPropertyAnimator(duration: 0.1, curve: .linear) {
            view.transform = scaledAndTranslatedTransform
        }
    }
    
    static func rotateLeft(view: UIView) -> UIViewPropertyAnimator {
        return UIViewPropertyAnimator(duration: 0.7, curve: .easeOut, animations: {
            view.transform = view.transform.rotated(by: -angle)
        })
    }
    
    static func rotateLRight(view: UIView) -> UIViewPropertyAnimator {
        return UIViewPropertyAnimator(duration: 0.7, curve: .easeOut, animations: {
            view.transform = view.transform.rotated(by: angle)
        })
    }
    
    
    static func moveLeft(view: UIView) -> UIViewPropertyAnimator {
        let moveShiftX = view.frame.width
        let moveShiftY = view.frame.width * sin(angle)
        let endPoint = CGPoint(x: view.center.x - moveShiftX,
                               y: view.center.y + moveShiftY)
        
        return UIViewPropertyAnimator(duration: 0.1, curve: .easeOut, animations: {
            view.center = endPoint
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.09, execute: {
                view.isHidden = true
            })
        })
    }
    
    static func moveRight(view: UIView)  -> UIViewPropertyAnimator {
        let moveShiftX = view.frame.width
        let moveShiftY = view.frame.width * sin(angle)
        let endPoint = CGPoint(x: view.center.x + moveShiftX,
                               y: view.center.y + moveShiftY)
        
        return UIViewPropertyAnimator(duration: 0.1, curve: .easeOut, animations: {
            view.center = endPoint
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.09, execute: {
                view.isHidden = true
            })
        })
    }
    
}
