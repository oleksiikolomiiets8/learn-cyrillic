//
//  Configuration.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/7/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var grey4: UIColor {
        return UIColor(red: 249.0 / 255.0, green: 249.0 / 255.0, blue: 249.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var iceBlue: UIColor {
        return UIColor(red: 236.0 / 255.0, green: 239.0 / 255.0, blue: 241.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var charcoalGrey: UIColor {
        return UIColor(red: 57.0 / 255.0, green: 71.0 / 255.0, blue: 78.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightGreyBlue: UIColor {
        return UIColor(red: 178.0 / 255.0, green: 190.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var greyBlue: UIColor {
        return UIColor(red: 100.0 / 255.0, green: 124.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var blueGrey: UIColor {
        return UIColor(red: 124.0 / 255.0, green: 144.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightBlueGrey: UIColor {
        return UIColor(red: 208.0 / 255.0, green: 216.0 / 255.0, blue: 220.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var ruby: UIColor {
        return UIColor(red: 213.0 / 255.0, green: 0.0 / 255.0, blue: 70.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var slimeGreen: UIColor {
        return UIColor(red: 151.0 / 255.0, green: 213.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var dark: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 50.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
    }
    
}
