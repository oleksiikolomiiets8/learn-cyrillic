//
//  LanguageType.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 2/25/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

enum LanguageType: String, CaseIterable, Codable, Equatable {
    case makedonian = "mkd"
    case russian    = "rus"
    case ukrainian  = "ukr"
    case belarusian = "blr"
    
    static public func instance(from data: Data) -> LanguageType? {
        return try? JSONDecoder().decode(LanguageType.self, from: data)
    }
    
    public var data: Data {
        return try! JSONEncoder().encode(self) as Data
    }
    
    var title: String {
        switch self {
        case .makedonian:
            return  "Makedonian".localized
        case .russian:
            return "Russian".localized
        case .ukrainian:
            return "Ukrainian".localized
        case .belarusian:
            return "Belarusian".localized
        }
    }
    
    var subtitle: String {
        if isActive {
            if isTested {
                let score = SymbolsHelper.learnedSymbolsCount(by: self)
                return String(format: "tested_subtitle".localized, score)
            } else {
                return String(format: "not_tested_sybtitle".localized, self.title)
            }
        } else {
            return "сomming_soon".localized
        }
    }
    
    var isTested: Bool {
        return SymbolsHelper.isLanguageTested(by: self)
    }
    
    var isActive: Bool {
        return self != .belarusian
    }
    
}
