//
//  ViewModel.swift
//  learn-cyrillic
//
//  Created by  Oleksii Kolomiiets on 3/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import Foundation

typealias ViewModelCallback = ((_ error: Error?) -> Void)?

protocol ViewModel {
    func subscribe(with updateCallback: ViewModelCallback)
}


