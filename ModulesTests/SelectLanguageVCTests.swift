//
//  SelectLanguageVCTests.swift
//  learn-cyrillicTests
//
//  Created by  Oleksii Kolomiiets on 4/8/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import XCTest
@testable import learn_cyrillic

class SelectLanguageVCTests: XCTestCase {

    var sut: SelectLanguageViewController!
    
    override func setUp() {
        sut = SelectLanguageViewController.instantiate()
        sut.viewModel = SingleSelectLanguageVMStub()
    }

    override func tearDown() {
        sut = nil
    }
    
    func testSelectLanguageVCSetupsIBOutletExpected() {
        //when
        sut.loadViewIfNeeded()
        //then
        XCTAssertNotNil(sut.tableView)
    }

    class SingleSelectLanguageVMStub: SelectLanguageViewModelProtocol {
        var languageCount: Int = 1
        
        func languageTitle(at indexPath: IndexPath) -> String { return "Title" }
        func languageSubtitle(at indexPath: IndexPath) -> String { return "Subtitle" }
        func languageFlag(at indexPath: IndexPath) -> UIImage?  { return nil }
        func languageIsActive(at indexPath: IndexPath) -> Bool  { return true }
        
        func selectLanguage(at indexPath: IndexPath)  { }
        func subscribe(with updateCallback: ViewModelCallback) { }
    }
}
