//
//  TestViewControllerTests.swift
//  learn-cyrillicTests
//
//  Created by  Oleksii Kolomiiets on 4/10/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import XCTest
@testable import learn_cyrillic

class TestViewControllerTests: XCTestCase {

    var sut: TestViewController!
    
    override func setUp() {
        sut = TestViewController.instantiate()
    }

    override func tearDown() {
        sut = nil
    }
}
