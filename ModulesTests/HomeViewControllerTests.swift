//
//  HomeViewControllerTests.swift
//  learn-cyrillicTests
//
//  Created by  Oleksii Kolomiiets on 4/8/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import XCTest
@testable import learn_cyrillic

class HomeViewControllerTests: XCTestCase {

    var sut: HomeViewController!
    
    override func setUp() {
        sut = HomeViewController.instantiate()
        sut.viewModel = MakedonianHomeVMStub()
    }

    override func tearDown() {
        sut = nil
    }
    
    func testHomeVCSetupsFrontAndBackCardsAsExpected() {
        //when
        sut.loadViewIfNeeded()
        //then
        XCTAssertNotNil(sut.frontSymbolCardView)
        XCTAssertNotNil(sut.backSymbolCardView)
    }
    
    func testHomeVCSetupsSymbolsBlockAsExpected() {
        //when
        sut.loadViewIfNeeded()
        //then
        XCTAssertNotNil(sut.symbolsStackView)
        XCTAssertNotNil(sut.previewLabel)
        XCTAssertNotNil(sut.leadingConstraint)
    }
    
    func testHomeVCSetupsTestButtonAsExpected() {
        //when
        sut.loadViewIfNeeded()
        //then
        XCTAssertNotNil(sut.testButton)
    }
    
    func testHomeVCSetupsHelpLabelsAsExpected() {
        //when
        sut.loadViewIfNeeded()
        //then
        XCTAssertNotNil(sut.yesLabel)
        XCTAssertNotNil(sut.noLabel)
    }
    
    func testHomeVCSetsUIAfterSubscribe() {
        //when
        sut.loadViewIfNeeded()
        sut.viewDidLoad()
        //then
        XCTAssertEqual(sut.frontSymbolCardView.mainLabel.text, "Ѓ  ѓ")
        XCTAssertEqual(sut.frontSymbolCardView.transliterationLabel .text, "gj")
        XCTAssertEqual(sut.frontSymbolCardView.pronunciationExampleLabel.text, "like gi in give; soft g")
        XCTAssertEqual(sut.frontSymbolCardView.originalExampleLabel.text, "Имаше ѓубре на подот")
        XCTAssertEqual(sut.frontSymbolCardView.translatedExampleLabel.text, "There was trash on the floor")
    }

}

class MakedonianHomeVMStub: HomeViewModelProtocol {
    var selectLanguage: (() -> Void) = { print("selectLanguage not overriden") }
    var testYourKnowledge: ((_ :LanguageType) -> Void) = { _ in print("testYourKnowledge not overriden") }
    var showInfo: (() -> Void) = { print("showInfo not overriden") }
    var lastCompletedTest: (() -> Void) = { print("lastCompletedTest not overriden") }
    
    var navBarButtonRatioSize: CGFloat = 24
    var symbolCount: Int = 3
    var currentSymbolIndex: Int = 2
    var normalSymbols: [String] = ["а", "б", "ѓ"]
    var languageImage: UIImage? = nil
    var difficaltyImage: UIImage? = nil
    var infoImage: UIImage? = nil
    var nextSymbolIndex: Int = 0
    
    func test() {}
    func loadSymbols() {}
    func toggleSymbolsOrder() {}
    func symbolCapital(at index: Int) -> String { return "Ѓ" }
    func symbolNormal(at index: Int) -> String { return "ѓ" }
    func symbolTransliteration(at index: Int) -> String { return "gj" }
    func symbolEnglishPronunciation(at index: Int) -> String { return "like gi in <b>gi</b>ve; soft g" }
    func originalExample(at index: Int) -> String { return "Имаше <b>ѓ</b>убре на подот" }
    func translatedExample(at index: Int) -> String { return "There was trash on the floor"}
    func learningState(at index: Int) -> LearningState { return .undisclosed }
    func updateSelectedIndex(_ index: Int) { }
    func changeLanguage(to language: LanguageType) { }
    func updateSymbolState(_ state: LearningState) { }
    func subscribe(with updateCallback: ViewModelCallback) { }
    
    
}
