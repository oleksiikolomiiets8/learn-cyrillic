//
//  TestSymbolTests.swift
//  learn-cyrillicTests
//
//  Created by  Oleksii Kolomiiets on 4/10/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import XCTest
@testable import learn_cyrillic

class TestSymbolTests: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSymbolCorrectInitialization() {
        //given
        let expectedVariants: [VariantProtocol] = [FirstCorrectVariantStub(),
                                                   SecondIncorrectVariantStub(),
                                                   ThirdIncorrectVariantStub()]
        
        //when
        let sut = TestSymbol(symbol: "А", variants: expectedVariants)
        let actualSymbol = sut.symbol
        let actualVariants = sut.variants
        
        //then
        XCTAssertEqual(actualSymbol, "А", "Symbol should be equal 'А' after initialization")
        XCTAssertFalse(actualSymbol == "B", "Symbol should not be equal 'B' after initialization")
        for (index, actualVariant) in actualVariants.enumerated() {
            let expectedVariant = expectedVariants[index]
            XCTAssertEqual(actualVariant.isCorrect, expectedVariant.isCorrect)
            XCTAssertEqual(actualVariant.value, expectedVariant.value)
        }
    }

    class FirstCorrectVariantStub: VariantProtocol {
        let value: String = "A"
        let isCorrect: Bool = true
    }
    
    class SecondIncorrectVariantStub: VariantProtocol {
        let value: String = "R"
        let isCorrect: Bool = false
    }
    
    class ThirdIncorrectVariantStub: VariantProtocol {
        let value: String = "B"
        let isCorrect: Bool = false
    }
}
