//
//  AboutVMTests.swift
//  learn-cyrillicTests
//
//  Created by  Oleksii Kolomiiets on 4/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import XCTest
@testable import learn_cyrillic

class AboutVMTests: XCTestCase {
    
    var sut: AboutViewModel!

    override func setUp() {
        sut = AboutViewModel()
    }

    override func tearDown() {
        sut = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testAboutSectionTypeInitialization() {
        let developmentRawValue = 0
        
        let expectingSection = AboutSectionType.development
        let actualSection = AboutSectionType(rawValue: developmentRawValue)
        
        XCTAssertEqual(expectingSection, actualSection)
    }
    
    func testDevelopmentSectionTitle() {
        let sectionType = AboutSectionType.development
        
        XCTAssertEqual(sectionType.title, "development".localized)
    }
    
    func testExpertsSectionTitle() {
        let sectionType = AboutSectionType.experts
        
        XCTAssertEqual(sectionType.title, "experts".localized)
    }
    
    func testSourceSectionTitle() {
        let sectionType = AboutSectionType.source
        
        XCTAssertEqual(sectionType.title, "source".localized)
    }
    
    func testHeaderHeight() {
        let height = sut.headerHeight
        let expectedHeight: CGFloat = 34.0
        
        XCTAssertEqual(height, expectedHeight, "Header height should be 34")
    }
    
    func testSectionsNumber() {
        let numberOfSections = sut.numberOfSections
        let expectedSections = AboutSectionType.allCases
        
        XCTAssertEqual(numberOfSections, expectedSections.count)
    }
    
    func testAppVersionTitleString() {
        let sutVersion = sut.version
        
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let expectedVersionTitle = String(format: "app_version".localized, versionNumber)
        
        XCTAssertEqual(sutVersion, expectedVersionTitle)
    }
    
    func testDevelopmentNumberOfRows() {
        let actualDevelopmentRowsNumber = sut.numberOfRows(at: AboutSectionType.development.rawValue)
        
        let expectedDevelopmentRows = Development.allCases
        
        XCTAssertEqual(actualDevelopmentRowsNumber, expectedDevelopmentRows.count, "rows")
    }
    
    func testExpertsNumberOfRows() {
        let actualExpertRowsNumber = sut.numberOfRows(at: AboutSectionType.experts.rawValue)
        
        let expectedExpertsRows = Experts.allCases
        
        XCTAssertEqual(actualExpertRowsNumber, expectedExpertsRows.count, "rows")
    }
    
    func testSourcesNumberOfRows() {
        let actualSourcesRowsNumber = sut.numberOfRows(at: AboutSectionType.source.rawValue)
        
        let expectedSourcesRows = Source.allCases
        
        XCTAssertEqual(actualSourcesRowsNumber, expectedSourcesRows.count, "rows")
    }
    
    func testIncorrectNumberOfRowsShouldBeZero() {
        let actualIncorrectRowsNumber = sut.numberOfRows(at: -1)
        
        let expectedIncorrectRowsCount = 0
        
        XCTAssertEqual(actualIncorrectRowsNumber, expectedIncorrectRowsCount, "Should return 0 rows")
        
    }

    func testDevSectionTitleShouldBeDevelopment() {
        let expectedTitle = "development".localized
        
        let actualitle = sut.sectionTitle(for: 0)
        
        XCTAssertEqual(actualitle, expectedTitle)
    }
    
    func testIncorrectSectionTitleShouldBeNil() {
        let incorrectSectionIndex = IndexPath(row: -1, section: -1)
        
        let incorrectTitle = sut.rowTitle(for: incorrectSectionIndex)
        
        XCTAssertEqual(incorrectTitle, nil)
    }

}
