//
//  TestCompleteVMTests.swift
//  learn-cyrillicTests
//
//  Created by  Oleksii Kolomiiets on 4/4/19.
//  Copyright © 2019 Olexander Markov. All rights reserved.
//

import XCTest
@testable import learn_cyrillic

class TestCompleteVMTests: XCTestCase {

    var sut: TestCompleteViewModel!
    
    override func setUp() {
        sut = TestCompleteViewModel()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testMakedonianTestCompleteCardTitle() {
        let expectedTitle = "Makedonian".localized + " " + "Cyrillic".localized
        
        sut.language = .makedonian
        let actualTitle = sut.testCompleteCardTitle
        
        XCTAssertEqual(actualTitle, expectedTitle)
    }

}
